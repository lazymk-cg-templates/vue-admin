/**
 * 示例模块
 * @author 懒猴子CG
 * @date 2020/04/28 09:36
 */
const state = {
  sampleStateContent: 'Hello (From Module State)'
}
const mutations = {}
const actions = {}
const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
